import 'package:flutter/material.dart';
import 'package:johnkonene/screens/FeatureTwo.dart';
import 'package:johnkonene/screens/profile.dart';

import 'featureOne.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true, title: const Text("Dashboard")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            OutlinedButton(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
              ),
              onPressed: () {
                Navigator.push(context,
              MaterialPageRoute(builder: (context) => const FeatureOne()));
              },
              child: const Text('Feature One'),
            ),
            const SizedBox(height: 25,),

            OutlinedButton(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.orange),
              ),
              onPressed: () {
                Navigator.push(context,
              MaterialPageRoute(builder: (context) => const FeatureTwo()));
              },
              child: const Text('Feature Two'),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const Profile()));
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
