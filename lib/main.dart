import 'package:flutter/material.dart';
import 'package:johnkonene/screens/splash.dart';

void main() => runApp(const App());

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      
      debugShowCheckedModeBanner: false,
      title: "Kuruman Records",
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home:const Splash(), 
      );

  }
}
