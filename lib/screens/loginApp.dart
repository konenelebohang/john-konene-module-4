import 'package:flutter/material.dart';

class LoginApp extends StatelessWidget {
  const LoginApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            "LOGIN",
          ),
        ),

        body: Padding(
            padding: const EdgeInsets.all(30),
              child: Center(
                child: SingleChildScrollView(
                 child: Column(
                      children: [
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                            decoration: const InputDecoration(
                              labelText: "Enter email",
                              border: OutlineInputBorder(),
                              prefixIcon: Icon(Icons.email),
                            ),
                        ),
                      
                        const SizedBox(
                            height: 25,
                        ),

                        TextFormField(
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: true,
                            decoration: const InputDecoration(
                              labelText: "Enter Password",
                              border: OutlineInputBorder(),
                              prefixIcon: Icon(Icons.lock),
                              suffixIcon: Icon(Icons.remove_red_eye),
                            ),
                        ),
              
                        const SizedBox(
                          height: 5,
                        ),
              
                      Row(
                            mainAxisAlignment:  MainAxisAlignment.end,
                        children: [
                          TextButton(onPressed: (){}, child: const Text(
                            "Forget Password?"
                            )
                          )
                        ],
                      ),

                    const SizedBox(
                      height: 25,
                    ),


                Container(
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Color(0x0ffC3C3C3),
                ),

                child: MaterialButton(
                  onPressed: (){},
                  child: const Text(
                    "LOGIN",
                    style: TextStyle(
                      fontSize: 25,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
             
              const SizedBox(
                height: 10,
              ),
              const Divider(
                height: 30,
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),

              Row(
                mainAxisAlignment:  MainAxisAlignment.center,
                children: [
                  Text(
                    "Don't have an Account?",
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.7),
                    ),

                  ),
                  TextButton(onPressed: (){},
                  child: const Text(
                      "Register Account"
                  ))
                ],
              ),


            ],
          ),
        ),
      ),
    )
    );
    
  }
}